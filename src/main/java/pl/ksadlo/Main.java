package pl.ksadlo;

import pl.ksadlo.error.NotAllowedOperationException;
import pl.ksadlo.service.OperationService;


import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        OperationService operationService = new OperationService();
        handleMenu(scan, operationService);
        try {
            operationService.saveState();
        } catch (IOException e) {
            System.out.println("Nie można zapisać stanu!");
        }
    }

    private static void handleMenu(Scanner scan, OperationService operationService) {
        printMainMenu();
        System.out.println("Wprowadź numer wybranej operacji:");
        int choice = scan.nextInt();
        if (choice == 1) {
            operationService.printUsers();
        } else if (choice == 2) {
            operationService.printUser(getUserId(scan));
        } else if (choice == 3) {
            operationService.printBalance(getUserId(scan));
        } else if (choice == 4) {
            try {
                operationService.withdraw(getUserId(scan), getAmount(scan));
            } catch (NotAllowedOperationException e) {
                System.out.println(e.getMessage());
            }
        } else if (choice == 5) {
            operationService.deposit(getUserId(scan), getAmount(scan));
        } else if (choice == 6) {
            try {
                operationService.transfer(getUserId(scan), getUserId(scan), getAmount(scan));
            } catch (NotAllowedOperationException e) {
                System.out.println(e.getMessage());
            }
        } else if (choice == 7) {
            operationService.deleteUser(getUserId(scan));
        } else if (choice == 8) {
            operationService.printTransactions(getUserId(scan));
        } else if (choice == 0) {
            return;
        } else {
            System.out.println("Rozwiązania nie wspierane");
        }
        handleMenu(scan, operationService);
    }

    private static int getUserId(Scanner scanner) {
        System.out.println("Wprowadź ID użytkownika:");
        return scanner.nextInt();
    }

    private static double getAmount(Scanner scanner) {
        System.out.println("Podaj kwotę:");
        return scanner.nextDouble();
    }

    private static void printMainMenu() {
        System.out.println("1. Wyświetl listę użytkowników banku");
        System.out.println("2. Wyświetl dane użytkownika");
        System.out.println("3. Wyświetl stan konta użytkownika");
        System.out.println("4. Wypłata pieniędzy z konta");
        System.out.println("5. Wpłata pieniędzy na konto");
        System.out.println("6. Przelew na konto");
        System.out.println("7. Zamknięcie konta");
        System.out.println("8. Historia transakcji");
        System.out.println("0. Zakończ program");
    }
}