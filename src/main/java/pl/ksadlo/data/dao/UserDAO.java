package pl.ksadlo.data.dao;

import pl.ksadlo.data.entity.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserDAO {
    private List<User> users;
    private final File usersFile = new File("accounts.csv");

    public UserDAO() throws IOException {
        this.users = getUsersFromFile();
    }

    public List<User> getAllUsers() {
        return users;
    }

    private List<User> getUsersFromFile() throws IOException {
        List<User> users = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(usersFile.toPath())) {
            while (true) {
                String line = reader.readLine();

                if (line == null) {
                    break;
                }
                String[] tab = line.split(",");
                User user = new User(Integer.parseInt(tab[0]), tab[1], tab[2]);
                users.add(user);
            }
        }
        return users;
    }

    public Optional<User> getUserById(int id) {
        return users.stream()
                .filter(user -> id == user.getId())
                .findFirst();
    }

    public void deleteUser(int userId) {
        users = users.stream()
                .filter(user -> userId != user.getId())
                .collect(Collectors.toList());
    }

    public void saveState() throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(usersFile.toPath())) {
            for (User user : users) {
                writer.write(user.getId() + "," + user.getName() + "," + user.getSurname());
                writer.newLine();
            }
        }
    }
}

