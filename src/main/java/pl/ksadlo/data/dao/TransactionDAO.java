package pl.ksadlo.data.dao;

import pl.ksadlo.data.entity.Transaction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionDAO {
    private List<Transaction> transactions;
    private final File transactionFile = new File("transactions.csv");

    public TransactionDAO() throws IOException {
        this.transactions = getTransactionFromFile();
    }

    private List<Transaction> getTransactionFromFile() throws IOException {
        List<Transaction> transactions = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(transactionFile.toPath())) {
            while (true) {
                String line = reader.readLine();

                if (line == null) {
                    break;
                }
                String[] tab = line.split(",");
                Transaction transaction = new Transaction(Integer.parseInt(tab[0]), tab[1],
                        LocalDateTime.parse(tab[2]));
                transactions.add(transaction);
            }
        }
        return transactions;
    }

    public List<Transaction> getTransactionsByUserId(int userId) {
        return transactions.stream()
                .filter(transaction -> userId == transaction.getUserId())
                .collect(Collectors.toList());
    }

    public void addTransaction(int userId, String description) {
        Transaction transaction = new Transaction(userId, description, LocalDateTime.now());
        transactions.add(transaction);
    }

    public void saveState() throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(transactionFile.toPath())) {
            for (Transaction transaction : transactions) {
                writer.write(transaction.getUserId() + "," + transaction.getDescription() + "," + transaction.getDate());
                writer.newLine();
            }
        }
    }
}
