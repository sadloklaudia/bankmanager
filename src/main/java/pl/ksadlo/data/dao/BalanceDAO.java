package pl.ksadlo.data.dao;

import pl.ksadlo.data.entity.Balance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BalanceDAO {
    private List<Balance> balances;
    private final File balancesFile = new File("balances.csv");

    public BalanceDAO() throws IOException {
        this.balances = getBalancesFromFile();
    }

    public List<Balance> getAllBalances() {
        return balances;
    }

    private List<Balance> getBalancesFromFile() throws IOException {
        List<Balance> balances = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(balancesFile.toPath())) {
            while (true) {
                String line = reader.readLine();

                if (line == null) {
                    break;
                }
                String[] tab = line.split(",");
                Balance balance = new Balance(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]),
                        Double.parseDouble(tab[2]));
                balances.add(balance);
            }
        }
        return balances;
    }

    public Optional<Balance> getBalanceByUserId(int userId) {
        return balances.stream()
                .filter(balance -> userId == balance.getUserId())
                .findFirst();
    }

    public void deleteBalanceByUserId(int userId) {
        Optional<Balance> balanceOptional = getBalanceByUserId(userId);
        if (balanceOptional.isEmpty()) {
            System.out.println("Stan konta nie istnieje");
            return;
        }
        if (balanceOptional.get().getValue() <= 0) {
            balances = balances.stream()
                    .filter(balance -> userId != balance.getUserId())
                    .collect(Collectors.toList());
        } else {
            System.out.println("Nie można usunąć konta z saldem dodatnim");
        }
    }

    public void saveState() throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(balancesFile.toPath())) {
            for (Balance balance : balances) {
                writer.write(balance.getId() + "," + balance.getUserId() + "," + balance.getValue());
                writer.newLine();
            }
        }
    }
}
