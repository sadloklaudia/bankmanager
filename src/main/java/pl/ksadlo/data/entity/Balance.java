package pl.ksadlo.data.entity;

public class Balance {
     private final int id;
     private final int userId;
     private double value;

     public Balance(int id, int userId, double value) {
          this.id = id;
          this.userId = userId;
          this.value = value;
     }

     public int getId() {
          return id;
     }

     public int getUserId() {
          return userId;
     }

     public double getValue() {
          return value;
     }

     public void setValue(double value) {
          this.value = value;
     }

     @Override
     public String toString() {
          return "Balance{" +
                  "id=" + id +
                  ", userId=" + userId +
                  ", value=" + value +
                  '}';
     }
}

