package pl.ksadlo.data.entity;

import java.time.LocalDateTime;

public class Transaction {
    private final int userId;
    private final String description;
    private final LocalDateTime date;

    public Transaction(int userId, String description, LocalDateTime date) {
        this.userId = userId;
        this.description = description;
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "userId=" + userId +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
