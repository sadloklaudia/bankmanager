package pl.ksadlo.error;

public class NotAllowedOperationException  extends Exception {
    public NotAllowedOperationException(String message) {
        super(message);
    }
}
