package pl.ksadlo.service;

import pl.ksadlo.data.dao.BalanceDAO;
import pl.ksadlo.data.dao.TransactionDAO;
import pl.ksadlo.data.dao.UserDAO;
import pl.ksadlo.data.entity.Balance;
import pl.ksadlo.data.entity.Transaction;
import pl.ksadlo.data.entity.User;
import pl.ksadlo.error.NotAllowedOperationException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class OperationService {
    private final BalanceDAO balanceDAO;
    private final UserDAO userDAO;
    private final TransactionDAO transactionDAO;

    public OperationService() {
        try {
            balanceDAO = new BalanceDAO();
            userDAO = new UserDAO();
            transactionDAO = new TransactionDAO();
        } catch (IOException e) {
            System.out.println("Nie można otworzyć pliku");
            throw new IllegalStateException();
        }
    }

    public void printUsers() {
        List<User> allUsers = userDAO.getAllUsers();
        allUsers.forEach(System.out::println);
    }

    public void printUser(int id) {
        Optional<User> user = userDAO.getUserById(id);
        if (user.isPresent()) {
            System.out.println(user.get());
        } else {
            System.out.println("Użytkownik nie istnieje");
        }
    }

    public void printBalance(int id) {
        Optional<Balance> balance = balanceDAO.getBalanceByUserId(id);
        if (balance.isPresent()) {
            System.out.println(balance.get());
        } else {
            System.out.println("Użytkownik nie istnieje");
        }
    }

    public void printTransactions(int userId) {
        List<Transaction> transactions = transactionDAO.getTransactionsByUserId(userId);
        transactions.forEach(System.out::println);
    }

    public void deleteUser(int id) {
        deleteBalance(id);
        userDAO.deleteUser(id);
    }

    public void deleteBalance(int userId) {
        balanceDAO.deleteBalanceByUserId(userId);
    }

    public void saveState() throws IOException {
        userDAO.saveState();
        balanceDAO.saveState();
        transactionDAO.saveState();
    }

    public void withdraw(int userId, double amount) throws NotAllowedOperationException {
        Optional<Balance> balanceOptional = balanceDAO.getBalanceByUserId(userId);
        if (balanceOptional.isPresent()) {
            Balance balance = balanceOptional.get();
            if (balance.getValue() < amount) {
                throw new NotAllowedOperationException("Brak wystarczających środków na koncie");
            }
            double newValue = balance.getValue() - amount;
            balance.setValue(newValue);
            transactionDAO.addTransaction(userId, "Wypłata środków: " + amount);
        } else {
            System.out.println("Stan konta dla użytkownika " + userId + " nie istnieje");
        }
    }

    public void deposit(int userId, double amount) {
        Optional<Balance> balanceOptional = balanceDAO.getBalanceByUserId(userId);
        if (balanceOptional.isPresent()) {
            Balance balance = balanceOptional.get();
            double newValue = balance.getValue() + amount;
            balance.setValue(newValue);
            transactionDAO.addTransaction(userId, "Wpłata środków: " + amount);
        } else {
            System.out.println("Brak stanu konta dla wybranego użytkownika");
        }
    }

    public void transfer(int fromUserId, int toUserId, double amount) throws NotAllowedOperationException {
        withdraw(fromUserId, amount);
        deposit(toUserId, amount);
        transactionDAO.addTransaction(fromUserId, "Przelew na rachunek użytkownika: " + toUserId + "o wartości: " + amount);
    }
}
